//soal-1
var daftarHewan = ["2. komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

console.log("\n=====> jawaban soal 1");
daftarHewan.sort();
daftarHewan.forEach(function (i) {
  console.log(i);
});

// soal-2
function introduce() {
  console.log(
    "Nama saya " +
      data.name +
      ", umur saya " +
      data.age +
      " tahun, alamat saya di " +
      data.address +
      ", dan saya punya hobby yaitu " +
      data.hobby
  );
}

var data = {
  name: "John",
  age: 30,
  address: "Jalan Pelesiran",
  hobby: "Gaming",
};

console.log("\n=====> jawaban soal 2");
var perkenalan = introduce(data);

console.log(perkenalan);

// Soal-3
var vowels = ["a", "e", "i", "o", "u"];

function hitung_huruf_vokal(str) {
  let count = 0;

  for (let letter of str.toLowerCase()) {
    if (vowels.includes(letter)) {
      count++;
    }
  }
  return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log("\n=====> jawaban soal 3");

console.log(hitung_1, hitung_2);

// soal-4
function hitung(angka) {
  return angka * 2 + -2;
}
console.log("\n=====> jawaban soal 4");

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
