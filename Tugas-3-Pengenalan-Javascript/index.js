// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
// jawaban soal 1
var kata1 = pertama.substring(0, 5);
var kata2 = pertama.substring(12, 19);
var kata3 = kedua.substring(0, 7);
var kata4 = kedua.toUpperCase().substring(7, 18);

var kalimat = kata1.concat(kata2).concat(kata3).concat(kata4);

console.log("\n=====> jawaban 1");
console.log(kalimat);

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// jawaban soal 2
var numPertama = Number(kataPertama);
var numKedua = Number(kataKedua);
var numKetiga = Number(kataKetiga);
var numKeempat = Number(kataKeempat);

var hasil = (numPertama % numKetiga) * numKeempat * numKedua;

console.log("\n=====> jawaban 2");
console.log(hasil);

// Soal 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3);
// jawab soal 3
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25);
console.log("\n=====> jawaban 3");

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
