// Soal 1

const persegiPanjang = (p, l) => {
  luas = p * l;
  keliling = (p + l) * 2;
  console.log("luas persegi panjang : " + luas);
  console.log("keliling persegi panjang : " + keliling);
};
console.log("\n=====> jawaban soal 1");

persegiPanjang(5, 4);

// Soal 2

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

//Driver Code
console.log("\n=====> jawaban soal 2");

newFunction("William", "Imoh").fullName();

// Soal 3

const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

// Driver code
console.log("\n=====> jawaban soal 3");
console.log(firstName, lastName, address, hobby);

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
//Driver Code
console.log("\n=====> jawaban soal 4");

console.log(combined);

// Soal 5

const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amer, consectetur adipiscing elit ${planet}`;
console.log("\n=====> jawaban soal 5");

console.log(before);
